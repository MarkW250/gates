﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    // Public properties
    [Tooltip("If gate should start open")]
    public bool StartOpen = false;

    // My objects
    private Animator _myAnimator;


    // State variables
    bool _open = false;

    // Start is called before the first frame update
    void Start()
    {
        _myAnimator = GetComponentInChildren<Animator>();


        _open = StartOpen;
        _myAnimator.SetBool("Open", _open);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // public methods
    public void SetState(bool open)
    {
        _open = open;
        _myAnimator.SetBool("Open", _open);
    }

    public void ToggleState()
    {
        _open = !_open;
        _myAnimator.SetBool("Open", _open);
    }
}
