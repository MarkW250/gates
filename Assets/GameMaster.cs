﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    // Public properties
    [Tooltip("Ball Spawning Rate")]
    public float SpawnRate = 2.0f;

    [Tooltip("Ball Prefab")]
    public Ball BallPrefab;

    // Private state variables
    private float _spawnTimer = 0.0f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(_spawnTimer >= SpawnRate)
        {
            Ball newBall = Instantiate(BallPrefab, new Vector3(0, 6, 0), new Quaternion());
            _spawnTimer = 0.0f;
            if(Random.value <= 0.5)
            {
                newBall.BallType = Ball.BallTypeT.Gold;
            }
            else
            {
                newBall.BallType = Ball.BallTypeT.Ruby;
            }
        }
        else
        {
            _spawnTimer += Time.deltaTime;
        }
    }
}
