﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // My Objects
    private List<Gate> _myGates = new List<Gate>();

    // Start is called before the first frame update
    void Start()
    {
        GetGateObjects();
    }

    // Update is called once per frame
    void Update()
    {
        bool openPressed = Input.GetKeyDown(KeyCode.Space);
        if(openPressed)
        {
            ToggleAllGates();
        }

        bool openReleased = Input.GetKeyUp(KeyCode.Space);
        if (openReleased)
        {
            ToggleAllGates();
        }

    }


    // Private Methods
    // Get gate objects in game
    private void GetGateObjects()
    {
        foreach(Gate gate in FindObjectsOfType<Gate>())
        {
            _myGates.Add(gate);
        }
    }

    // Open all gates
    private void SetAllGates(bool state)
    {
        foreach (Gate gate in _myGates)
        {
            gate.SetState(state);
        }
    }

    // Open all gates
    private void ToggleAllGates()
    {
        foreach (Gate gate in _myGates)
        {
            gate.ToggleState();
        }
    }
}
