﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    // Public properties
    [Tooltip("Starting speed of ball")]
    public float StartingSpeed = 1.0f;

    // Public types
    public enum BallTypeT
    {
        Gold,
        Ruby
    }

    // My Objects
    private Rigidbody2D _myRigidBody;
    private SpriteRenderer _mySprite;

    // Private Properties
    private BallTypeT _myType;

    // Start is called before the first frame update
    void Start()
    {
        _myRigidBody = GetComponent<Rigidbody2D>();
        _mySprite = GetComponentInChildren<SpriteRenderer>();

        Vector2 startSpeed = new Vector2(0, StartingSpeed * -1.0f);
        _myRigidBody.velocity = startSpeed;

        setColorForType();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Gate")
        {
            Destroy(this.gameObject);
        }

        if (collision.gameObject.tag == "Goal")
        {
            Destroy(this.gameObject);
        }
    }

    // Public Methods
    public BallTypeT BallType 
    {
        get
        {
            return _myType;
        }
        set
        {
            _myType = value;
        }
    }


    // Private Methods
    private void setColorForType()
    {
        Color newcolor = new Color();
        newcolor.a = 1;
        switch (_myType)
        {
            case BallTypeT.Gold:
                newcolor.r = 0.89f;
                newcolor.g = 0.89f;
                newcolor.b = 0.07f;
                break;

            case BallTypeT.Ruby:
                newcolor.r = 1;
                newcolor.g = 0;
                newcolor.b = 0;
                break;

            default:
                Debug.LogError("Unknown Ball Type!");
                break;
        }

        _mySprite.color = newcolor;
    }
}
